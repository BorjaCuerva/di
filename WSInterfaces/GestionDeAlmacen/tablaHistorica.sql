create database if not exists interfaces;
use interfaces;
create table if not exists tablaHistorica (
cliente varchar(6) not null,
proveedor varchar(6) not null,
articulo varchar(6) not null,
unidades float not null,
fecha date not null
);

/*ALTER TABLE tablaHistorica ADD FOREIGN KEY(cliente) REFERENCES clientes(codigo);*/
/*ALTER TABLE tablaHistorica ADD FOREIGN KEY(proveedor) REFERENCES proveedores(codigo);*/
/*ALTER TABLE tablaHistorica ADD FOREIGN KEY(articulo) REFERENCES articulos(codigo);*/
