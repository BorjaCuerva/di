/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondealmacen;

/**
 *
 * @author Borja
 */
public class Cliente {
	
	String codigo;
	String nif;
	String letraNif;
	String apellidos;
	String nombre;
	String domicilio;
	String cp;
	String localidad;
	String telefono;
	String movil;
	String fax;
	String email;
	float totalVentas;
	
	
	//CONSTRUCTOR
	
	public Cliente(String codigo, String nif, String letraNif, String apellidos, String nombre, String domicilio, String cp, String localidad, String telefono, String movil, String fax, String email, float totalVentas) {
		this.codigo = codigo;
		this.nif = nif;
		this.letraNif = letraNif;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.cp = cp;
		this.localidad = localidad;
		this.telefono = telefono;
		this.movil = movil;
		this.fax = fax;
		this.email = email;
		this.totalVentas = totalVentas;
	}
	
	
	//CONSTRUCTOR SIN LETRANIF
	public Cliente(String codigo, String nif, String apellidos, String nombre, String domicilio, String cp, String localidad, String telefono, String movil, String fax, String email, float totalVentas) {
		this.codigo = codigo;
		this.nif = nif;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.cp = cp;
		this.localidad = localidad;
		this.telefono = telefono;
		this.movil = movil;
		this.fax = fax;
		this.email = email;
		this.totalVentas = totalVentas;
	}
	
	//CONSTRUCTOR PARA COMPROBAR SI EXISTE CLIENTE
	public Cliente(String codigo) {
		this.codigo = codigo;
	}
	
	public Cliente(){
		
	}
	
	
	
	//GETTERS AND SETTERS

	public String getCodigo() {
		return codigo;
	}

	public String getNif() {
		return nif;
	}

	public String getLetraNif() {
		return letraNif;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public String getCp() {
		return cp;
	}

	public String getLocalidad() {
		return localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getMovil() {
		return movil;
	}

	public String getFax() {
		return fax;
	}

	public String getEmail() {
		return email;
	}

	public float getTotalVentas() {
		return totalVentas;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public void setLetraNif(String letraNif) {
		this.letraNif = letraNif;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTotalVentas(float totalVentas) {
		this.totalVentas = totalVentas;
	}
	
	
	
	
	
}
