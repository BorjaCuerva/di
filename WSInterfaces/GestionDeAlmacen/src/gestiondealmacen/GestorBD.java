/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestiondealmacen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Borja
 */
public class GestorBD {

    Connection con;

    public GestorBD() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Conectamos a la BBDD mysql
            //Para conectar en casa
            //con = DriverManager.getConnection("jdbc:mysql://localhost/interfaces", "root", "");
            //Para conectar en clase
            con = DriverManager.getConnection("jdbc:mysql://localhost/interfaces", "root", "manager");
            //Ponemos autoCommit a false
            con.setAutoCommit(false);
            //JOptionPane.showMessageDialog(null, "CONECTADO!!!", "CONECTADO!!!", 1);
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Error con el driver", "Driver", 1);
            e.printStackTrace();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al conectar a la BBDD", "BBDD", 1);
            ex.printStackTrace();
        }

    }

    /**
     * Metodo para consultar un cliente. Lo utilizamos a la hora de dar un alta,
     * una baja o una modificacion. Consultamos el cliente por el codigo, que
     * devuelve un cliente con todos sus datos.
     *
     * @param codigo --> Clave primaria de la BBDD
     * @return --> Retorna un objeto Cliente con todos los datos del mismo.
     * @throws Exception
     */
    public Cliente consultarCliente(String codigo) throws SQLException {

        String nif;
        String apellidos;
        String nombre;
        String domicilio;
        String cp;
        String localidad;
        String telefono;
        String movil;
        String fax;
        String email;
        float totalVentas;

        Cliente c1 = null;
        //Cambiar el persona por tabla
        String sqlBusqueda = "SELECT * FROM clientes WHERE codigo= " + "'" + codigo + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sqlBusqueda);

        //Guardamos en un objeto de tipo persona todos los datos
        while (rs.next()) {

            nif = rs.getString("nif");
            apellidos = rs.getString("apellidos");
            nombre = rs.getString("nombre");
            domicilio = rs.getString("domicilio");
            cp = rs.getString("codigoPostal");
            localidad = rs.getString("localidad");
            telefono = rs.getString("telefono");
            movil = rs.getString("movil");
            fax = rs.getString("fax");
            email = rs.getString("email");
            totalVentas = rs.getFloat("totalVentas");

            c1 = new Cliente(codigo, nif, apellidos, nombre, domicilio, cp, localidad, telefono, movil, fax, email, totalVentas);

        }

        return c1;
    }

    /**
     * Metodo para comprobar si existe un cliente en la BBDD.
     *
     * @param sql --> consulta para comprobar el codigo
     * @param codigo --> Codigo a comparar con la BBDD, si esta en la BBDD el
     * cliente existe
     * @return --> True si existe en la BBDD // False si no existe en la BBDD
     * @throws SQLException
     */
    public boolean existeCliente(String sql, String codigo) throws SQLException {

        //El sql que viene por parametro, tiene una ? en codigo, para ello hacemos un PreparedStatement
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, codigo);
        ResultSet rs = ps.executeQuery();
        return rs.next();

    }

    public Cliente datosCliente(String codigo) throws SQLException {

        String nif = "";
        String nombre = "";
        String apellidos = "";
        String domicilio = "";
        String cp = "";
        String localidad = "";
        String telefono = "";
        String movil = "";
        String fax = "";
        String email = "";
        float totalVentas;

        Cliente cliente = null;

        String sql = "Select * from clientes where codigo = ?";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, codigo);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            nif = rs.getString("nif");
            apellidos = rs.getString("apellidos");
            nombre = rs.getString("nombre");
            domicilio = rs.getString("domicilio");
            cp = rs.getString("codigoPostal");
            localidad = rs.getString("localidad");
            telefono = rs.getString("telefono");
            movil = rs.getString("movil");
            fax = rs.getString("fax");
            email = rs.getString("email");
            totalVentas = rs.getFloat("totalVentas");

            cliente = new Cliente(codigo, nif, nombre, apellidos, domicilio, cp, localidad, telefono, movil, fax, email, totalVentas);

        }

        return cliente;
    }

    /**
     * Guarda cliente en la BBDD. Hay que hacer commit al final para que se
     * guarde, ya que autocommit esta en false
     *
     * @param c --> Cliente
     * @throws Exception
     */
    public void guardarCliente(Cliente c) throws Exception {

        Cliente cliente = consultarCliente(c.getCodigo());

        if (cliente == null) {
            String sql = "INSERT INTO clientes VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, c.getCodigo());
            pst.setString(2, c.getNif());
            pst.setString(3, c.getNombre());
            pst.setString(4, c.getApellidos());
            pst.setString(5, c.getDomicilio());
            pst.setString(6, c.getCp());
            pst.setString(7, c.getLocalidad());
            pst.setString(8, c.getTelefono());
            pst.setString(9, c.getMovil());
            pst.setString(10, c.getFax());
            pst.setString(11, c.getEmail());
            pst.setFloat(12, c.getTotalVentas());
            pst.executeUpdate();
            pst.close();

        } else {
            JOptionPane.showMessageDialog(null, "El cliente con el codigo " + cliente.getCodigo() + " ya existe.", "Error al crear cliente", 1);
        }

    }

    public void borrarCliente(Cliente c) throws SQLException {

        Cliente cliente = consultarCliente(c.getCodigo());
        if (cliente != null) {
            String sql = "DELETE FROM clientes WHERE codigo= ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, c.getCodigo());
            ps.executeUpdate();
            ps.close();

        } else {
            JOptionPane.showMessageDialog(null, "El cliente con el codigo " + cliente.getCodigo() + " no existe.", "Error al borrar cliente", 1);
        }
    }

    void modificarCliente(Cliente c) throws SQLException {
        Cliente cliente = consultarCliente(c.getCodigo());
        String codigo = c.getCodigo();

        String sql = "update clientes set nif = ?, nombre = ?, apellidos = ?, domicilio = ?, codigoPostal = ?, localidad = ?,"
                + "telefono = ?, movil = ?, fax = ?, email = ? where codigo = " + codigo;
        if (cliente != null) {

            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, c.getNif());
            pst.setString(2, c.getNombre());
            pst.setString(3, c.getApellidos());
            pst.setString(4, c.getDomicilio());
            pst.setString(5, c.getCp());
            pst.setString(6, c.getLocalidad());
            pst.setString(7, c.getTelefono());
            pst.setString(8, c.getMovil());
            pst.setString(9, c.getFax());
            pst.setString(10, c.getEmail());
            pst.executeUpdate();
            pst.close();

        } else {
            JOptionPane.showMessageDialog(null, "El cliente con el codigo " + cliente.getCodigo() + " no existe.", "Error al modificar cliente", 1);
        }

    }

    /**
     * Metodo para hacer commit en la clase FormularioClientes
     *
     * @throws SQLException
     */
    public void commit() throws SQLException {
        con.commit();
    }

    /**
     * Metodo para hacer rollBack en la clase FormularioClientes
     *
     * @throws SQLException
     */
    public void rollBack() throws SQLException {
        con.rollback();
    }

    /**
     * Metodo para generar un informe con JasperReports
     *
     * @return informe generado
     */
    public JasperViewer ejecutarInforme() {
        JasperViewer vistaInforme = null;
        JasperReport informeCargado;
        try {

            String archivoJasper = "/media/Datos/Borja/Repositorios/di/WSInterfaces/GestionDeAlmacen/ConsultasPorCodigo.jasper"; //El archivo tiene que estar a la altura de src
            informeCargado = (JasperReport) JRLoader.loadObject(archivoJasper);
            JasperPrint jasperPrint = JasperFillManager.fillReport(informeCargado, null, con);
            vistaInforme = new JasperViewer(jasperPrint, true);

        } catch (JRException e) {
            System.out.println(e.getMessage());
        }
        return vistaInforme;
    }
    
    public JasperViewer informeQuesos() {
        JasperViewer vistaInforme=null;
            JasperReport informeCargado;
            try 
            {   
                String archivoJasper = "graficoQuesos.jasper";
                    informeCargado = (JasperReport)JRLoader.loadObject(archivoJasper);
                    JasperPrint jasperPrint = JasperFillManager.fillReport(informeCargado,null,con);
                    vistaInforme= new JasperViewer(jasperPrint, true); 

            }
            catch (JRException e)
            {
                System.out.println(e.getMessage());
            }
            return vistaInforme;
    }
    
    
    
}
