create database if not exists interfaces;
use interfaces;
create table if not exists articulos (
codigo varchar(6) primary key,
descripcion varchar(25) not null,
stock float not null,
stockMinimo float not null,
precioDeCompra float not null,
precioDeVenta float not null
);
