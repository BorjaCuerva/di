/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 * @author Borja
 */

public class Operaciones {
    
    
    //Metodo sumar
    static double sumar (double numero1, double numero2) {

        return numero1+numero2;
		
        
    }
    
    //Metodo restar
    static double restar (double numero1, double numero2) {
        
        return numero1-numero2;
        
    }
    
    //Metodo multiplicar
    static double multiplicar (double numero1, double numero2) {
        
        return numero1*numero2;
        
    }
    
    //Metodo dividir
    static double dividir (double numero1, double numero2) {
       
        return numero1/numero2;
        
    }

    
    
}
