<%--
En esta ventana, si el usuario ejecuta el enlace Página principal, regresará a la ventana de
pagina principal y comenzará un nuevo pedido o solicitará un extracto de los pedidos
realizados entre dos fechas. 

Esta acción la añadiremos posteriormente.

Si el usuario pulsa el enlace aquí, se cargará una nueva página desde la que podrá imprimir
el albarán. Para ello, deberá seguir las instrucciones dadas al pricipio de la pagina.
--%>

<%@page import="objetos.PedidoInternet"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="gestorBD.GestorBD"%>
<%@page import="objetos.Cliente"%>
<%@page import="objetos.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body>
        <h2>Pedido finalizado</h2>
        Si desea imprimir el pedido se abrirá una nueva ventana con la 
        factura a imprimir.
        <br>
        En esta ventana, abra el menú Archivo y ejecute la opción Imprimir.
        <br>
        En la nueva ventana, seleccione su impresora y pulse Imprimir.
        <br>
        Después, cierre la ventana que contiene la factura a imprimir.
        <br>
        Para imprimir el pedido pulse <a href="imprimirPedido.jsp" target="blank">aquí.</a> Si no va a imprimir, puede regresar a la <a href="index.jsp">página principal.</a>
        <br><br>
        <%
            HttpSession sesion = request.getSession();
            ArrayList<Pedido> pedidos = (ArrayList<Pedido>) sesion.getAttribute("pedidos");
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            Pedido pedido = new Pedido();
            ArrayList<PedidoInternet> pedidosInternet = new ArrayList<>();
            GestorBD bd = (GestorBD) sesion.getAttribute("conexion");

            // Fecha actual
            java.sql.Date fechaSistema = new java.sql.Date((new java.util.Date()).getTime());
            String fecha = String.valueOf(fechaSistema);
            
            int numFactura = 0;
            if (sesion.getAttribute("numFactura") == null) {
                numFactura = 1;
                sesion.setAttribute("numFactura", numFactura);
            } else {
                numFactura = (Integer) sesion.getAttribute("numFactura");
                numFactura++;
                sesion.setAttribute("numFactura", numFactura);
            }
        
            int i = 0;
            int numLinea = 571;
            float total = 0;
            while (i < pedidos.size()) {
                
                pedido = (Pedido) pedidos.get(i);
                PedidoInternet pedidoInternet = new PedidoInternet();
                pedidoInternet.setCliente(cliente.getCodigo());
                pedidoInternet.setArticulo(pedido.getArticulo());
                pedidoInternet.setUnidades(pedido.getUnidades());
                pedidoInternet.setFecha(fecha);
                pedidosInternet.add(pedidoInternet);
                // Imprimo las cabeceras.
                if (numLinea > 570) {
                    /* La página se escribe en el pie de página. Por lo tanto,
                     si es la primera página, el número de página no se
                     escribe. */
                    numLinea = 30;
        %>
        <hr>
        Fecha:  <%= fecha%>
        <br>
        FACTURA N.: <%= numFactura%>
        <br>
        <hr>
    <center>
        <h2>Empresa Proyecto Web de clase, S.A.</h2>
        <h3>N.I.F.: 28938475-J</h3>
        <h4>C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h4>
    </center>
    <hr>
    <b>Cliente: </b><%= cliente.getCodigo()%> 
    <b>N.I.F.: </b><%= cliente.getNif()%>
    <br>
    <b>D./Dña. </b>
    <%= cliente.getNombre()%> <%= cliente.getApellidos()%>
    <br>
    <%= cliente.getDomicilio()%>
    <br>
    <%= cliente.getCp()%> <%= cliente.getLocalidad()%>
    <hr>
    <table align=center>
        <tr>
            <td><h3>Código</h3></td>
            <td><h3>Descripción</h3></td>
            <td><h3>Precio</h3></td>
            <td><h3>Unidades</h3></td>
            <td><h3>Importe</h3></td>
        </tr>
        <%
            }
        %>
        <tr>
            <td><%= pedido.getArticulo()%></td>
            <td><%= pedido.getDescripcion()%></td>
            <td align=right><%= String.valueOf(pedido.getPrecio())%></td>
            <td align=right><%= String.valueOf(pedido.getUnidades())%></td>
            <td align=right><%= String.valueOf(pedido.getImporte())%></td>
        </tr>
        <%
                total = total + pedido.getImporte();
                i++;
                
            }
        %>
    </table>
    <hr>
    <h2 align=right>Total Factura (I.V.A. inc.): <%= String.valueOf(total)%></h2>
    <hr>
    <%
        // Guardo los pedidos en la BBDD
        bd.guardarPedidosInternet(pedidosInternet);
        bd.guardarTablas();
    %>
</body>
</html>
