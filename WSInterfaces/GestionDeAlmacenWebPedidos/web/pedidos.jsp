<%--
En esta ventana, se muestran los datos del cliente y del artículo que el cliente ha solicitado.

Estos datos, hay que pasarlos de la página anterior.

En esta página, guardaremos el nuevo pedido en el atributo del objeto HttpSession creado previamente.

Si el usuario ejecuta el enlace Página principal, regresará a la pagina index.

Si el usuario desea pedir otros artículos, ejecutará el enlace Pedir otro artículo,
mostrándose la ventana anterior.

Cuando introduzca el nuevo artículo y las unidades, se mostrará con todos los artículos solicitados.

Si el usuario pulsa el botón Aceptar pedido, se cargará una nueva página en la que
guardaremos los pedidos en la nueva tabla que nos hemos creado y daremos opción al
usuario de imprimir el albarán con el pedido realizado.

Cuando los pedidos se graben en la tabla, se inicializarán los atributos del objeto
HttpSession para guardar los nuevos pedidos que se generen.
--%>

<%@page import="objetos.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page import="objetos.Articulo"%>
<%@page import="objetos.Cliente"%>
<%@page import="gestorBD.GestorBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body onload="document.formPedidos.txtUnidades.focus()">
        <h1>Gestión de pedidos</h1>
        <%
            HttpSession sesion = request.getSession();
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            ArrayList<Pedido> pedidos = (ArrayList<Pedido>) sesion.getAttribute("pedidos");
            // Creo un objeto de tipo Pedido para almacenar el pedido.
            Pedido pedido = new Pedido();
            pedido.setArticulo(request.getParameter("txtArticulo"));
            pedido.setDescripcion(request.getParameter("txtDescripcion"));
            Float precio = new Float(request.getParameter("txtPrecio"));
            pedido.setPrecio(precio.floatValue());
            Float unidades = new Float(request.getParameter("txtUnidades"));
            pedido.setUnidades(unidades.floatValue());
            Float importe = new Float(request.getParameter("txtImporte"));
            pedido.setImporte(importe.floatValue());
            pedidos.add(pedido);
            sesion.setAttribute("pedidos", pedidos);
        %>
        <!-- Muestro los datos del cliente -->
        <h2>Datos del cliente</h2>
        <table>
            <tr>
                <td><b>Código</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;N.I.F.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Nombre</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Apellidos</b></td>                
            </tr>
            <tr>
                <td><%= cliente.getCodigo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNif()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNombre()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getApellidos()%></td>
            </tr>
            <tr>
                <td><b>Domicilio</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;C.P.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Localidad</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Total</b></td>
            </tr>
            <tr>
                <td><%= cliente.getDomicilio()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getCp()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getLocalidad()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getTotalVentas()%></td>
            </tr>
        </table>
        <br>
        <hr>
        <br>
        <!-- Muestro los datos de todos los pedidos -->
        <h2>Pedido realizado</h2>
        <table>
            <tr>
                <td><b>Artículo</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Descripción</b></td>
                <td align="right"><b>&nbsp;&nbsp;&nbsp;Unidades</b></td>
                <td align="right"><b>&nbsp;&nbsp;&nbsp;Precio</b></td>
                <td align="right"><b>&nbsp;&nbsp;&nbsp;Importe</b></td>
            </tr>
            <%
                int i = 0;
                float total = 0;
                while (i < pedidos.size()) {
                    pedido = (Pedido) pedidos.get(i);
            %>
            <tr>
                <td><%= pedido.getArticulo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= pedido.getDescripcion()%></td>
                <td align="right">&nbsp;&nbsp;&nbsp;<%= pedido.getUnidades()%></td>
                <td align="right">&nbsp;&nbsp;&nbsp;<%= pedido.getPrecio()%></td>
                <td align="right">&nbsp;&nbsp;&nbsp;<%= pedido.getImporte()%></td>
                <%
                        total = total + pedido.getImporte();
                        i++;
                    }
                %>
        </table>
        <br>
        <h2>Importe del pedido: <%= total%></h2>
        <hr>
        <br>
        <form name="formPedidos" action="factura.jsp" method="get">
            <input type=hidden name=opcion value="<%= request.getParameter("opcion")%>">
            <input type="submit" name="aceptaPedidor" value="Aceptar pedido">
        </form>
        <br>
        <a href="buscarCliente.jsp?opcion=<%=request.getParameter("opcion")%>&amp;txtCodigo=<%= cliente.getCodigo()%>">Pedir otro artículo</a>
        |
        <a href="index.jsp">Página principal</a>
    </body>
</html>
