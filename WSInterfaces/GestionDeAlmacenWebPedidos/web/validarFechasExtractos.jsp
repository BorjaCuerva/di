<%@page import="java.util.ArrayList"%>
<%@page import="objetos.PedidoInternet"%>
<%@page import="objetos.Articulo"%>
<%@page import="objetos.Cliente"%>
<%@page import="gestorBD.GestorBD"%>
<%--
La nueva página que se cargue, podrá tener dos aspectos (HTML dinámico), dependiendo
de la existencia o inexistencia de pedidos entre las fechas seleccionadas. Si no hay ningún
pedido entre estas fechas, se mostrará un mensaje de que no hay extractos.

Desde esta ventana, ejecutando los enlaces insertados en la misma, podemos introducir un
nuevo cliente, o regresar a la página principal.

Si el usuario pulsa el botón Nuevas fechas, accederemos a la pagina anterior para poder seleccionar
nuevas fechas.

Si hay pedidos entre las fechas seleccionadas, se mostrará la lista con los pedidos realizados en
entre esas fechas.

En esta ventana, si el usuario ejecuta el enlace Página principal, regresará a la pagina principal
y comenzará un nuevo pedido o solicitará un nuevo extracto entre otras dos fechas.

Si, el usuario pulsa el enlace aquí, se cargará una nueva página desde la que podrá imprimir 
el extracto de los pedidos. Para ello, deberá seguir las instrucciones dadas.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <h1>Gestión de extractos</h1>
        <%
            HttpSession sesion = request.getSession();
            GestorBD bd = (GestorBD) sesion.getAttribute("conexion");
            Cliente cliente = (Cliente) session.getAttribute("cliente");

            String calendarioDesde = request.getParameter("calendarioDesde");
            String calendarioHasta = request.getParameter("calendarioHasta");
            sesion.setAttribute("calendarioDesde", calendarioDesde);
            sesion.setAttribute("calendarioHasta", calendarioHasta);

            ArrayList<PedidoInternet> listaPedidos = bd.consutarFechasPedidos(calendarioDesde, calendarioHasta, cliente.getCodigo());
            sesion.setAttribute("extractos", listaPedidos);
        %>
        <!-- Muestro los datos del cliente -->
        <h2>Datos del cliente</h2>
        <table>
            <tr>
                <td><b>Código</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;N.I.F.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Nombre</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Apellidos</b></td>                
            </tr>
            <tr>
                <td><%= cliente.getCodigo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNif()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNombre()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getApellidos()%></td>
            </tr>
            <tr>
                <td><b>Domicilio</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;C.P.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Localidad</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Total</b></td>
            </tr>
            <tr>
                <td><%= cliente.getDomicilio()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getCp()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getLocalidad()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getTotalVentas()%></td>
            </tr>
        </table>
        <br>
        <hr>
        <%
            if (!listaPedidos.isEmpty()) {
        %>
        <h2>Extracto de pedidos</h2>
        <h3>Del <%= calendarioDesde%> al <%= calendarioHasta%></h3>
        <br>
        <table>
            <tr>
                <td><b>Fecha</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Artículo</b></td>
                <td style="text-align: right"><b>&nbsp;&nbsp;&nbsp;Unidades</b></td>
            </tr>
            <%
                String fecha = "";
                String articulo = "";
                String unidades = "";
                // Bulce para leer todos los extractos y mostrarlos
                for (PedidoInternet pedido : listaPedidos) {
                    fecha = String.valueOf(pedido.getFecha());
                    articulo = pedido.getArticulo();
                    unidades = String.valueOf(pedido.getUnidades());
            %>
            <tr>
                <td><%= fecha%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= articulo%></td>
                <td style="text-align: right">&nbsp;&nbsp;&nbsp;<%= unidades%></td>
            </tr>
            <%
                }
            %>
        </table>
        <br>
        <hr>
        <h2>Extracto finalizado</h2>
        Si desea imprimir el extracto se abrirá una nueva ventana con el extracto a imprimir.
        <br>
        En esta ventana, abra el menú Archivo y ejecute la opción Imprimir.
        <br>
        En la nueva ventana, seleccione su impresora y pulse Imprimir.
        <br>
        Después, cierre la ventana que contiene el extracto a imprimir.
        <br>
        Para imprimir el extracto pulse <a href="imprimirExtracto.jsp" target="blank">aquí.</a> Si no va a imprimir, puede regresar a la <a href="index.jsp">página principal.</a>
        <br><br>
        <%
        } else {
        %>
        <!-- Muestro que no se han realizado pedidos en las fechas seleccionadas -->
        <h2>No ha hecho pedidos entre las fechas<br><br> <%= calendarioDesde%> y <%= calendarioHasta%></h2>
        <hr>
        <br>
        <form name="formExtractos" action="buscarClienteExtractos.jsp?opcion=<%=request.getParameter("opcion")%>&amp;txtCodigo=<%= cliente.getCodigo()%>" method="post">
            <input type="submit" name="botonNuevasFechas" value="Nuevas fechas"/>
        </form>
        <br>
        <a href="gestionExtractos.jsp">Nuevo cliente</a>
        | 
        <a href="index.jsp">Página principal</a>
        <%
            }
        %>
    </body>
</html>
