package gestorBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import objetos.Cliente;

import objetos.Articulo;
import objetos.PedidoInternet;
import objetos.Proveedor;

/**
 * Clase GestorDB. En esta clase crearemos un constructor para gestionar la Base
 * de Datos y las consultas necesarias para la gestion de la BD.
 */
public class GestorBD implements Persistencia {

    Connection conexion;
    // Tablas BBDD
    String tablaClientes = "clientes";
    String tablaProveedores = "proveedores";
    String tablaArticulos = "articulos";
    String tablaHistorica = "historica";
    String tablaPedidosInternet = "pedidosInternet";
    // Parametros conexion BBDD
    String ip = "localhost";
    String bd = "almacenweb";
    String usuario = "root";
    String password = "";

    /**
     * Metodo conectarBD. En este metodo conectamos la BD con SGBD de MySQL. En
     * la cadena de conexion le pasamos los parametros que tenemos declarados
     * antes arriba, tambien los podriamos pasar como parametros si quisieramos
     * recibirlos de otro lado.
     */
    @Override
    public void conectarDB() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Cadena de conexion
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + bd, usuario, password);
            // Ponemos que la BD no se guarde automaticamente
            conexion.setAutoCommit(false);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se encontro el Driver MySQL para JDBC.", "Aviso", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexión con la BD.", "Aviso", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

    }

    /**
     * Metodo desconectarBD. En este medoto desconectamos la BD.
     */
    @Override
    public void desconectarDB() {
        try {
            conexion.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la desconexión de la BD.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo consultarCliente. En este metodo consultamos si existe el cliente,
     * comprobando el codigo que le pasamos como parametros, hacemos un select
     * para ver si ese codigo existe en la BBDD. Si existe le devolvemos el
     * cliente de ese codigo si no null.
     *
     * @param codigo
     * @return
     */
    @Override
    public Cliente consultarCliente(String codigo) {
        String sql = "SELECT * FROM " + tablaClientes + " WHERE codigo = ?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, codigo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Cliente c = new Cliente(rs.getString("codigo"), rs.getString("nif"), rs.getString("apellidos"), rs.getString("nombre"), rs.getString("domicilio"), rs.getString("cp"), rs.getString("localidad"), rs.getString("telefono"), rs.getString("movil"), rs.getString("fax"), rs.getString("email"), rs.getFloat("total_ventas"));
                return c;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al consultar el Cliente.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    /**
     * Metodo consultarProveedor. En este metodo consultamos si existe el
     * proveedor, comprobando el codigo que le pasamos como parametros, hacemos
     * un select para ver si ese codigo existe en la BBDD. Si existe le
     * devolvemos el proveedor de ese codigo si no null.
     *
     * @param codigo
     * @return
     */
    public Proveedor consultarProveedor(String codigo) {
        String sql = "SELECT * FROM " + tablaProveedores + " WHERE codigo = ?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, codigo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Proveedor p = new Proveedor(rs.getString("codigo"), rs.getString("nif"), rs.getString("apellidos"), rs.getString("nombre"), rs.getString("domicilio"), rs.getString("cp"), rs.getString("localidad"), rs.getString("telefono"), rs.getString("movil"), rs.getString("fax"), rs.getString("email"), rs.getFloat("total_compras"));
                return p;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al consultar el Proveedor.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    /**
     * Metodo consultarArticulo. En este metodo consultamos si existe el
     * articulo, comprobando el codigo que le pasamos como parametros, hacemos
     * un select para ver si ese codigo existe en la BBDD. Si existe le
     * devolvemos el articulo de ese codigo si no null.
     *
     * @param codigo
     * @return
     */
    public Articulo consultarArticulo(String codigo) {
        String sql = "SELECT * FROM " + tablaArticulos + " WHERE codigo = ?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, codigo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Articulo a = new Articulo(rs.getString("codigo"), rs.getString("descripcion"), rs.getFloat("stock"), rs.getFloat("stockMinimo"), rs.getFloat("precioCompra"), rs.getFloat("precioVenta"));
                return a;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al consultar el Artículo.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    /**
     * Metodo altaCliente. En este metodo insertamos el cliente que recibimos
     * como parametro en la BD. Para hacer el insert he utilizado
     * PreparedStatement.
     *
     * @param c
     */
    @Override
    public void altaCliente(Cliente c) {
        String sql = "INSERT INTO " + tablaClientes + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, c.getCodigo());
            ps.setString(2, c.getNif());
            ps.setString(3, c.getApellidos());
            ps.setString(4, c.getNombre());
            ps.setString(5, c.getDomicilio());
            ps.setString(6, c.getCp());
            ps.setString(7, c.getLocalidad());
            ps.setString(8, c.getTelefono());
            ps.setString(9, c.getMovil());
            ps.setString(10, c.getFax());
            ps.setString(11, c.getEmail());
            ps.setFloat(12, c.getTotalVentas());
            ps.executeUpdate();
            // Guardamos el insert en la BD
            conexion.commit();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al dar de alta al Cliente.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo modificarCliente. En este metodo modificaremos el cliente que
     * recibimos como parametro. Para ello haremos un UPDATE con
     * PreparedStatement.
     *
     * @param c
     */
    @Override
    public void modificarCliente(Cliente c) {
        String sql = "UPDATE " + tablaClientes + " SET nif=?, apellidos=?, nombre=?, domicilio=?, cp=?, localidad=?, telefono=?, movil=?, fax=?, email=? WHERE codigo=?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, c.getNif());
            ps.setString(2, c.getApellidos());
            ps.setString(3, c.getNombre());
            ps.setString(4, c.getDomicilio());
            ps.setString(5, c.getCp());
            ps.setString(6, c.getLocalidad());
            ps.setString(7, c.getTelefono());
            ps.setString(8, c.getMovil());
            ps.setString(9, c.getFax());
            ps.setString(10, c.getEmail());
            ps.setString(11, c.getCodigo());
            ps.executeUpdate();
            // Guardamos la modificacion el BD
            conexion.commit();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al modificar al Cliente.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo bajaCliente. En este metodo vamos a dar de baja el cliente que
     * recibimos como parametro. Para ello vamos a eliminarlo de la BD con un
     * DELETE y un PreparedStatement.
     *
     * @param c
     */
    @Override
    public void bajaCliente(Cliente c) {
        String sql = "DELETE FROM " + tablaClientes + " WHERE codigo=?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, c.getCodigo());
            ps.executeUpdate();
            // Guardamos la elimancion del cliente
            conexion.commit();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al dar de baja al Cliente.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Metodo actualizarTablaTotal. En este metodo vamos a actualizar el total
     * del cliente o proveedor cuando realice una compra. Modificamos por el
     * codigo de cliente o proveedor.
     *
     * @param tabla
     * @param codigo
     * @param importe
     * @param total
     */
    public void actualizarTablaTotal(String tabla, String codigo, Float importe, String total) {
        String sql = "UPDATE " + tabla + " SET " + total + "=? WHERE codigo=?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setFloat(1, importe);
            ps.setString(2, codigo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al modificar el Total.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo actualizarTablaArticulos. En este metodo vamos a actualizar el
     * Stock del articulo con el codigo pasado por parametro.
     *
     * @param codigo
     * @param stockTotal
     */
    public void actualizarTablaArticulos(String codigo, float stockTotal) {
        String sql = "UPDATE " + tablaArticulos + " SET stock=? WHERE codigo=?";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setFloat(1, stockTotal);
            ps.setString(2, codigo);
            ps.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al modificar el Stock.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo guardarTablas. En este metodo realizo el commit para guardar los
     * procesos realizados en la BBDD.
     */
    public void guardarTablas() {
        try {
            conexion.commit();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar las tablas.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo deshacerCambios. En este metodo realizo un rollback.
     */
    public void deshacerCambios() {
        try {
            conexion.rollback();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al deshacer los cambios.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo guardarHistorico. En este metdodo se grabará un registro por cada
     * pedido (un pedido es: un cliente o proveedor, un artículo), conteniendo
     * cada registro el código del cliente o del proveedor, el código del
     * artículo, las unidades pedidas y la fecha del pedido. Los campos que
     * almacenan los códigos de cliente, proveedor y artículo. Para ello vamos a
     * hacer un insert con los parametros pasados.
     *
     * @param codigoCliente
     * @param codigoProveedor
     * @param codigoArticulo
     * @param unidades
     * @param fechaActual
     */
    public void guardarHistorico(String codigoCliente, String codigoProveedor, String codigoArticulo, float unidades, Date fechaActual) {
        String sql = "INSERT INTO " + tablaHistorica + " VALUES (?,?,?,?,?)";
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, codigoCliente);
            ps.setString(2, codigoProveedor);
            ps.setString(3, codigoArticulo);
            ps.setFloat(4, unidades);
            ps.setTimestamp(5, new Timestamp(fechaActual.getTime()));
            ps.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al crear el histórico.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo obtenerDatosArticulos. En este metodo obtenemos los datos de la
     * tabla articulos, los pasamos a un ArrayList y devolvemos el ArrayList con
     * los articulos.
     *
     * @return
     */
    public ArrayList<Articulo> obtenerDatosArticulos() {
        ArrayList<Articulo> listaArticulos = new ArrayList<>();
        String sql = "SELECT * FROM " + tablaArticulos;
        try {
            PreparedStatement ps = conexion.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Articulo a = new Articulo(rs.getString("codigo"), rs.getString("descripcion"), rs.getFloat("stock"), rs.getFloat("stockMinimo"), rs.getFloat("precioCompra"), rs.getFloat("precioVenta"));
                listaArticulos.add(a);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al consultar los Artículos.", "Aviso", JOptionPane.ERROR_MESSAGE);
        }
        return listaArticulos;
    }

    /**
     * Metodo guardarPedidosInternet. En este metodo obtenemos como parametro un
     * ArrayList de PedidoInternet, recoremos el Array y vamos haciendo un
     * insert de cada pedido en la tabla de pedidosInternet.
     *
     * @param listaPedidos
     */
    public void guardarPedidosInternet(ArrayList<PedidoInternet> listaPedidos) {
       

        try {
            for (PedidoInternet pedido : listaPedidos) {
                String sql = "INSERT INTO " + tablaPedidosInternet + " VALUES (?,?,?,?)";
                PreparedStatement ps = conexion.prepareStatement(sql);
                ps.setString(1, pedido.getCliente());
                ps.setString(2, pedido.getArticulo());
                ps.setFloat(3, pedido.getUnidades());
                ps.setString(4, pedido.getFecha());
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al crear el pedido de internet.", "Aviso", JOptionPane.ERROR_MESSAGE);
            System.out.println(ex.getMessage());
        }
    }
    
     /**
     * En este metodo consultamos los pedidos del
     * cliente que pasamos como parametro, mediante las fechas que le pasamos
     * como parametro. Devolvemos un ArrayList con los pedidos dentro de las
     * fechas introducidas.
     *
     * @param calendarioDesde
     * @param calendarioHasta
     * @param codigoCliente
     * @return
     */
    public ArrayList<PedidoInternet> consutarFechasPedidos(String calendarioDesde, String calendarioHasta, String codigoCliente) {
        ArrayList<PedidoInternet> listaPedidos = new ArrayList<>();
        String sql = "SELECT * FROM " + tablaPedidosInternet + " WHERE cliente = '" + codigoCliente + "' AND fecha BETWEEN '" + calendarioDesde + "' AND '" + calendarioHasta + "'";
        Statement st;
        try {
            st = conexion.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                listaPedidos.add(new PedidoInternet(codigoCliente, rs.getString("articulo"), rs.getInt("unidades"), rs.getString("fecha")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al consultar los pedidos de internet." + ex.getMessage(), "Aviso", JOptionPane.ERROR_MESSAGE);
        }
        return listaPedidos;
    }
 
}
