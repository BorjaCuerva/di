 -- Tabla Clientes
 CREATE TABLE clientes(
 codigo VARCHAR(6) PRIMARY KEY,
 nif VARCHAR(9),
 apellidos VARCHAR(35),
 nombre VARCHAR(15),
 domicilio VARCHAR(40),
 cp VARCHAR(5),
 localidad VARCHAR(20),
 telefono VARCHAR(9),
 movil VARCHAR(9),
 fax VARCHAR(9),
 email VARCHAR(20),
 total_ventas FLOAT
 );

-- Tabla Provedores
CREATE TABLE proveedores(
 codigo VARCHAR(6) PRIMARY KEY,
 nif VARCHAR(9),
 apellidos VARCHAR(35),
 nombre VARCHAR(15),
 domicilio VARCHAR(40),
 cp VARCHAR(5),
 localidad VARCHAR(20),
 telefono VARCHAR(9),
 movil VARCHAR(9),
 fax VARCHAR(9),
 email VARCHAR(20),
 total_compras FLOAT
 );
 
 -- Tabla Articulo
 CREATE TABLE articulos(
 codigo VARCHAR(6) PRIMARY KEY,
 descripcion VARCHAR(25),
 stock FLOAT,
 stockMinimo FLOAT,
 precioCompra FLOAT,
 precioVenta FLOAT
 );
 
 -- Tabla Historica
 CREATE TABLE historica (
	cliente varchar(6) NULL,
	proveedor varchar(6) NULL,
	articulo varchar(6) NULL,
	unidades FLOAT NULL,
	fecha DATE NULL,
	CONSTRAINT historica_clientes_fk FOREIGN KEY (cliente) REFERENCES almacen.clientes(codigo),
	CONSTRAINT historica_articulos_fk FOREIGN KEY (articulo) REFERENCES almacen.articulos(codigo),
	CONSTRAINT historica_proveedores_fk FOREIGN KEY (proveedor) REFERENCES almacen.proveedores(codigo)
);

 -- Tabla PedidoInternet
 CREATE TABLE pedidosInternet(
 cliente VARCHAR(6) NULL,
 articulo VARCHAR(6) NULL,
 unidades FLOAT NULL,
 fecha DATE NULL
 );

 -- Clientes
INSERT INTO clientes VALUES ('000001','70000001L','Garcia','Alex','Las pozas','28200','San Lorenzo','91897080','','687489634','',0);
INSERT INTO clientes VALUES ('000002','70000002L','Garcia','Carlos','La retama','28220','Escorial','91897083','','','cliente2@gmail.com',0);
INSERT INTO clientes VALUES ('000003','70000003L','Garcia','Pedro','Santa Rosa','28040','Madrid','91897084','','','cliente3@gmail.com',0);
INSERT INTO clientes VALUES ('000004','70000004L','Canales','Juan','La avenida','28080','Madird','91897085','','','',0);
 
 -- Proveedores
INSERT INTO proveedores VALUES ('000001','70000001L','Perez','Alberto','Las pozas','28200','San Lorenzo','91897082','','687489634','Email',0);
INSERT INTO proveedores VALUES ('000002','70000002L','Martinez','David','La retama','28220','Escorial','91897083','','','EmailProveedor3',0);
INSERT INTO proveedores VALUES ('000003','70000003L','Garcia','Miguel','Los pastores','28040','Madrid','91897084','','','Email',0);
INSERT INTO proveedores VALUES ('000004','70000004L','Canales','Alvaro','La avenida','28080','Madird','91897085','','','Proveedor5@gmail.com',0);

 -- Articulos
INSERT INTO articulos VALUES ('000001','Naranjas',10,5,2,3);
INSERT INTO articulos VALUES ('000002','Peras',20,2,3,3);
INSERT INTO articulos VALUES ('000003','Limones',30,3,1,2);
INSERT INTO articulos VALUES ('000004','Tomates',50,4,2,3);
INSERT INTO articulos VALUES ('000005','Sandias',10,1,2,3);
INSERT INTO articulos VALUES ('000006','Pomelo',25,3,4,6);

