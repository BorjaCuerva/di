package objetos;

/**
 * Clase Pedido. En esta clase crearemos un objeto Pedido con los parametros de
 * un pedido.
 *
 * @author Alex
 * @version 31/01/2019
 */
public class Pedido {

    // Paramentros
    private String articulo;
    private String descripcion;
    private float precio;
    private float unidades;
    private float importe;

    // Constuctor defecto
    public Pedido() {
    }

    // Constructor
    public Pedido(String articulo, String descripcion, float precio, float unidades, float importe) {
        this.articulo = articulo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.unidades = unidades;
        this.importe = importe;
    }

    // Getters and Setters
    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getUnidades() {
        return unidades;
    }

    public void setUnidades(float unidades) {
        this.unidades = unidades;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

}
