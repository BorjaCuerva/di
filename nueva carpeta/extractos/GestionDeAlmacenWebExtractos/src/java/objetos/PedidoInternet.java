package objetos;

import java.util.Date;

/**
 * Clase PedidoInternet. En esta clase crearemos un objeto PedidoInternet con
 * los parametros de la tabla pedidosInternet.
 *
 * @author Alex
 * @version 31/01/2019
 */
public class PedidoInternet {

    // Parametros
    private String cliente;
    private String articulo;
    private float unidades;
    private Date fecha;

    // Constructor defecto
    public PedidoInternet() {
    }

    // Constructor
    public PedidoInternet(String cliente, String articulo, float unidades, Date fecha) {
        this.cliente = cliente;
        this.articulo = articulo;
        this.unidades = unidades;
        this.fecha = fecha;
    }

    //Getters and Setters
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public float getUnidades() {
        return unidades;
    }

    public void setUnidades(float unidades) {
        this.unidades = unidades;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
