<%-- 
    Document   : buscarCliente
    Created on : 22-ene-2019, 10:56:16
    Author     : Alex
--%>
<%-- 
La nueva página que se cargue, podrá tener dos aspectos (HTML dinámico), dependiendo
de la existencia o inexistencia del código. Si el cliente no existe, se mostrará que no existe el cliente.

Desde esta ventana, ejecutando los enlaces insertados en la misma, podemos introducir un
nuevo cliente, volviendo a la pagina anterior, o regresar a la página principal.

Si el cliente introducido en la ventana existe, se mostrará esta ventana, se puede introducir el código
del artículo que se va a pedir, introducir un nuevo cliente, volviendo a la pagina anterior, o regresar a la página principal.

Cuando se cargue esta página, se mostrarán los datos del cliente y el foco se enviará a la
caja de texto del Artículo.

El botón Aceptar es un botón submit y el botón Cancelar es un botón reset.

Cuando el usuario pulse el botón Cancelar, el foco se enviará a la caja de texto del Artículo.

Si el usuario no ha introducido ningún dato y pulsa el botón Aceptar, se visualizará un
mensaje de error y continuaremos en la misma página.

Cuando el usuario haya introducido un artículo y pulse el botón Aceptar, se cargará una
nueva página en el navegador.
--%>

<%@page import="objetos.Cliente"%>
<%@page import="gestorBD.GestorBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body onload="document.formPedidos.txtArticulo.focus()">
        <h1>Gestión de pedidos</h1>
        <%
            String codigo = request.getParameter("txtCodigo");
            HttpSession sesion = request.getSession();
            GestorBD bd = (GestorBD) sesion.getAttribute("conexion");
            bd.conectarDB();
            Cliente cliente = bd.consultarCliente(codigo);
            sesion.setAttribute("cliente", cliente);
            if (cliente != null) {
        %>
        <!-- Muestro los datos del cliente -->
        <h2>Datos del cliente</h2>
        <table>
            <tr>
                <td><b>Código</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;N.I.F.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Nombre</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Apellidos</b></td>                
            </tr>
            <tr>
                <td><%= cliente.getCodigo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNif()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNombre()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getApellidos()%></td>
            </tr>
            <tr>
                <td><b>Domicilio</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;C.P.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Localidad</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Total</b></td>
            </tr>
            <tr>
                <td><%= cliente.getDomicilio()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getCp()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getLocalidad()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getTotalVentas()%></td>
            </tr>
        </table>
        <br>
        <hr>
        <br>
        <h2>Realizar pedido</h2>
        Artículo
        <br>
        <form name="formPedidos" action="buscarArticulo.jsp" method="post" onsubmit="return correctoSubmit()">
            <input type="hidden" name=opcion value="<%= request.getParameter("opcion")%>">
            <input type="text" name="txtArticulo" pattern="[A-z0-9]{0,6}" size="4" maxlength="6">
            <br><br>
            <input type="submit" value="Aceptar">
            <input type="reset" value="Cancelar" 
                   onclick="document.formPedidos.txtArticulo.focus()">                    
            <br><br>
            <hr>
        </form>
        <%
        } else {
        %>
        <!-- Muestro que el cliente no existe -->
        <h2>El cliente con código  <%= codigo%> no existe.</h2>
        <%
            }
        %>
        <br>
        <a href="gestionPedidos.jsp">Nuevo cliente</a>
        | 
        <a href="index.jsp">Página principal</a>
        <!-- Script para la comprobacion del articulo -->
        <script type="text/javascript">
            // Metodo para comprobar que se ha introducido un numero
            function correctoSubmit() {
                with (document.formPedidos.txtArticulo) {
                    if (document.formPedidos.txtArticulo.value === "") {
                        alert("No ha introducido ningún artículo");
                        focus();
                        return false;
                    } else {
                        for (i = 1; value.length < 6; i++) {
                            value = "0" + value;
                        }
                        return true;
                    }
                }
            }
        </script>
    </body>
</html>
