<%-- 
    Document   : imprimirExtracto
    Created on : 05-feb-2019, 10:50:16
    Author     : Alex
--%>
<%@page import="objetos.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="objetos.PedidoInternet"%>
<%--
En esta ventana, el usuario imprimirá su extracto desde el cuadro de diálogo
Imprimir del menú Archivo. El listado que saldrá por su impresora será exactamente igual al
contenido de esta ventana.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession();
            ArrayList<PedidoInternet> listaExtractos = (ArrayList<PedidoInternet>) sesion.getAttribute("extractos");
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            String calendarioDesde = (String) sesion.getAttribute("calendarioDesde");
            String calendarioHasta = (String) sesion.getAttribute("calendarioHasta");
            /* La siguiente instrucción coge la fecha del sistema. Indico el 
             paquete de la clase Date() que utilizo para que no se confunda 
             entre la clase Date() del paquete java.sql y la clase Date() 
             del paquete java.util. */
            java.sql.Date fechaSistema = new java.sql.Date((new java.util.Date()).getTime());
            /* Convierto la fecha del sistema a String para guardarlo en la 
             tabla PedidosInternet, donde he definido el campo Fecha de tipo 
             Text. */
            String fecha = String.valueOf(fechaSistema);
            /* Le doy la vuelta a la fecha para imprimirla en el listado de la 
             factura. */
            String fecha1 = fecha.substring(8, 10);
            fecha1 = fecha1 + fecha.substring(4, 7);
            fecha1 = fecha1 + "-" + fecha.substring(0, 4);
        %>
        Fecha:  <%= fecha1%>
        <br>
        <hr>
        <h2 style="text-align: center">Empresa Proyecto Web de clase, S.A.</h2>
        <h3 style="text-align: center">N.I.F.: 28938475-J</h3>
        <h4 style="text-align: center">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h4>
        <hr>
        <b>Cliente: </b><%= cliente.getCodigo()%> 
        <b>N.I.F.: </b><%= cliente.getNif()%>
        <br>
        <b>D./Dña. </b>
        <%= cliente.getNombre()%> <%= cliente.getApellidos()%>
        <br>
        <%= cliente.getDomicilio()%>
        <br>
        <%= cliente.getCp()%> <%= cliente.getLocalidad()%>
        <hr>
        <h2 style="text-align: center">Extracto de pedidos desde <%=calendarioDesde%> hasta <%=calendarioHasta%></h2>
        <br>
        <table style=" margin: auto; text-align: center;">
            <tr>
                <td><h3>Fecha</h3></td>
                <td><h3>Unidades</h3></td>
                <td><h3>Artículo</h3></td>
            </tr>	
            <% for (PedidoInternet pedido : listaExtractos) {%>
            <tr>
                <td><%= pedido.getFecha()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%=pedido.getUnidades()%></td>
                <td style="text-align: right">&nbsp;&nbsp;&nbsp;<%=pedido.getArticulo()%></td>
            </tr>
            <% }%>
        </table>
        <hr>
        <br><br>
    </body>
</html>
