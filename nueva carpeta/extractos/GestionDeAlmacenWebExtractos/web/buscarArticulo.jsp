<%-- 
    Document   : buscarArticulo
    Created on : 31-ene-2019, 11:00:23
    Author     : Alex
--%>
<%--
La nueva página que se cargue, podrá tener dos aspectos (HTML dinámico), dependiendo
de la existencia o inexistencia del artículo. Si no existe, se mostrará que el articulo no existe.

Desde esta ventana, ejecutando los enlaces insertados en la misma, podemos introducir un
nuevo artículo, volviendo a la ventana anterior, o regresar a la página principal.

Si el artículo, introducido en la ventana de la Figura 5.1.4, existe, se mostrará la ventana, se 
pueden introducir las unidades que se van a pedir del artículo solicitado, introducir un nuevo artículo, 
volviendo a la ventana de la Figura 5.1.4, o regresar a la página principal.

Cuando se cargue esta página, el foco se enviará a la caja de texto de las Unidades.

Según se vayan introduciendo las unidades, se irá calculando el importe y escribiéndolo en
la caja de texto del Importe. Si se introduce un carácter que no sea numérico, se visualizará
un mensaje de error, se borrará el contenido de la caja de texto Unidades y esta misma
caja recibirá el foco.

El botón Aceptar es un botón submit y el botón Cancelar es un botón reset.
Cuando el usuario pulse el botón Cancelar, el foco se enviará a la caja de texto de las
Unidades.

Como vemos en esta ventana, además de mostrar algunos datos del artículo solicitado, se
siguen mostrando los datos del cliente que está realizando el pedido.

Si el usuario no ha introducido ningún dato y pulsa el botón Aceptar, se visualizará un
mensaje de error y continuaremos en la misma página.

Cuando el usuario haya introducido las unidades correctamente y pulse el botón Aceptar,
se cargará una nueva página en el navegador.
--%>

<%@page import="objetos.Cliente"%>
<%@page import="objetos.Articulo"%>
<%@page import="gestorBD.GestorBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body onload="document.formPedidos.txtUnidades.focus()">
        <h1>Gestión de pedidos</h1>
        <%
            String codigoArticulo = request.getParameter("txtArticulo");
            HttpSession sesion = request.getSession();
            GestorBD bd = (GestorBD) sesion.getAttribute("conexion");
            Cliente cliente = (Cliente) session.getAttribute("cliente");
            Articulo articulo = bd.consultarArticulo(codigoArticulo);
            sesion.setAttribute("articulo", articulo);
            if (articulo != null) {
        %>
        <!-- Muestro los datos del cliente -->
        <h2>Datos del cliente</h2>
        <table>
            <tr>
                <td><b>Código</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;N.I.F.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Nombre</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Apellidos</b></td>                
            </tr>
            <tr>
                <td><%= cliente.getCodigo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNif()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNombre()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getApellidos()%></td>
            </tr>
            <tr>
                <td><b>Domicilio</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;C.P.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Localidad</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Total</b></td>
            </tr>
            <tr>
                <td><%= cliente.getDomicilio()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getCp()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getLocalidad()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getTotalVentas()%></td>
            </tr>
        </table>
        <br>
        <hr>
        <br>
        <h2>Realizar pedido</h2>
        <br>
        <form name="formPedidos" action="pedidos.jsp" method="get" onsubmit="return correctoSubmit()">
            <table>
                <tr>
                    <td><b>Artículo</b></td>
                    <td><b>&nbsp;&nbsp;&nbsp;Descripción</b></td>
                    <td><b>&nbsp;&nbsp;&nbsp;Unidades</b></td>
                    <td><b>&nbsp;&nbsp;&nbsp;Precio</b></td>
                    <td><b>&nbsp;&nbsp;&nbsp;Importe</b></td>
                </tr>
                <tr>
                    <td><%= request.getParameter("txtArticulo")%></td>
                    <td>&nbsp;&nbsp;&nbsp;<%= articulo.getDescripcion()%></td>
                    <td>&nbsp;&nbsp;<input type="text" name="txtUnidades" required pattern="^[1-9][0-9]{0,5}$" size="5" maxlength="5" onkeyup="calcularPrecio()"></td>
                    <td>&nbsp;&nbsp;&nbsp;<%= articulo.getPrecioVenta()%></td>
                    <td>&nbsp;&nbsp;<input type="text" name="txtImporte" size="9" maxlength="9" readonly></td>
                </tr>
            </table>
            <!-- Oculto las cajas de texto para pasarlas a la siguiente página -->
            <input type="hidden" name="txtArticulo" value="<%= request.getParameter("txtArticulo")%>">                    
            <input type="hidden" name="txtDescripcion" value="<%= articulo.getDescripcion()%>">
            <input type="hidden" name="txtPrecio" value="<%= articulo.getPrecioVenta()%>">
            <input type="hidden" name="opcion" value="<%= request.getParameter("opcion")%>">
            <br><br>
            <input type="submit" value="Aceptar">
            <input type="reset" value="Cancelar" onclick="document.formPedidos.txtUnidades.focus()">                    
            <br><br>
            <hr>
        </form>
        <%
        } else {
        %>
        <!-- Muestro que el articulo no existe -->
        <h2>El artículo con código <%= codigoArticulo%> no existe.</h2>
        <%
            }
        %>
        <br>
        <a href="buscarCliente.jsp?opcion=<%=request.getParameter("opcion")%>&amp;txtCodigo=<%= cliente.getCodigo()%>">Nuevo artículo</a>
        | 
        <a href="index.jsp">Página principal</a>
        <!-- Script para la comprobacion del articulo -->
        <script type="text/javascript">
            // Metodo para calcular el precio de las unidades y comprobar que sean solo numeros
            function calcularPrecio() {
                // Calculo el importe del pedido.
                importe = document.formPedidos.txtUnidades.value * document.formPedidos.txtPrecio.value;
                document.formPedidos.txtImporte.value = importe;
            }
            // Metodo para comprobar que se ha introducido un numero
            function correctoSubmit() {
                with (document.formPedidos.txtUnidades) {
                    if (document.formPedidos.txtUnidades.value === "") {
                        alert("No ha introducido las unidades");
                        focus();
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        </script>
    </body>
</html>
