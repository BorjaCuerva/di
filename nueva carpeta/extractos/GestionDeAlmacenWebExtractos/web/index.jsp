<%-- 
    Document   : index
    Created on : 21-ene-2019, 10:08:30
    Author     : Alex
--%>

<%@page import="objetos.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page import="gestorBD.GestorBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    // Creamos un HttpSession con la conexion a la BBDD
    HttpSession httpSession = request.getSession(true);
    // Creamos el GestorBD
    GestorBD bd = new GestorBD();
    // Aniadimos el GestorBD al HttpSesion 
    httpSession.setAttribute("conexion", bd);
    // Array para guardar los pedidos
    httpSession.setAttribute("pedidos", new ArrayList<Pedido>());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página principal</title>
    </head>
    <body>
        <h1>Proyecto Web con Base de Datos</h1>
        <a href="gestionPedidos.jsp">Gestión de Pedidos</a>
        <br><br>
        <a href="gestionExtractos.jsp">Gestión de extractos</a>
        <br><br>
        <a href="GestionDeAlmacen.jar">Proyecto</a>
    </body>
</html>
