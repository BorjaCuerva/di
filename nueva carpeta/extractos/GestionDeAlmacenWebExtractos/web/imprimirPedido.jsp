<%-- 
    Document   : imprimirPedido
    Created on : 31-ene-2019, 21:52:30
    Author     : Alex
--%>

<%@page import="objetos.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="objetos.Pedido"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession();
            ArrayList<Pedido> pedidos = (ArrayList<Pedido>) sesion.getAttribute("pedidos");
            Cliente cliente = (Cliente) sesion.getAttribute("cliente");
            Pedido pedido = new Pedido();
            /* La siguiente instrucción coge la fecha del sistema. Indico el 
             paquete de la clase Date() que utilizo para que no se confunda 
             entre la clase Date() del paquete java.sql y la clase Date() 
             del paquete java.util. */
            java.sql.Date fechaSistema = new java.sql.Date((new java.util.Date()).getTime());
            /* Convierto la fecha del sistema a String para guardarlo en la 
             tabla PedidosInternet, donde he definido el campo Fecha de tipo 
             Text. */
            String fecha = String.valueOf(fechaSistema);
            /* Le doy la vuelta a la fecha para imprimirla en el listado de la 
             factura. */
            String fecha1 = fecha.substring(8, 10);
            fecha1 = fecha1 + fecha.substring(4, 7);
            fecha1 = fecha1 + "-" + fecha.substring(0, 4);
            Integer numFactura = (Integer) sesion.getAttribute("numFactura");
            int i = 0;
            int numLinea = 571;
            float total = 0;
            float importe = 0;
            while (i < pedidos.size()) {
                /* Recupero los pedidos del vector del atributo Pedidos del 
                 objeto Session. */
                pedido = (Pedido) pedidos.get(i);
                // Imprimo las cabeceras.
                if (numLinea > 570) {
                    /* La página se escribe en el pie de página. Por lo tanto,
                     si es la primera página, el número de página no se
                     escribe. */
                    numLinea = 30;
        %>
        <hr>
        Fecha:  <%= fecha1%>
        <br>
        FACTURA N.: <%= numFactura%>
        <br>
        <hr>
        <h2 style="text-align: center">Empresa Proyecto Web de clase, S.A.</h2>
        <h3 style="text-align: center">N.I.F.: 28938475-J</h3>
        <h4 style="text-align: center">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h4>
        <hr>
        <b>Cliente: </b><%= cliente.getCodigo()%> 
        <b>N.I.F.: </b><%= cliente.getNif()%>
        <br>
        <b>D./Dña. </b>
        <%= cliente.getNombre()%> <%= cliente.getApellidos()%>
        <br>
        <%= cliente.getDomicilio()%>
        <br>
        <%= cliente.getCp()%> <%= cliente.getLocalidad()%>
        <hr>
        <table style=" margin: auto; text-align: center;">
            <tr>
                <td><h3>Código</h3></td>
                <td><h3>Descripción</h3></td>
                <td><h3>Precio</h3></td>
                <td><h3>Unidades</h3></td>
                <td><h3>Importe</h3></td>
            </tr>
            <%
                }
            %>
            <tr>
                <td><%= pedido.getArticulo()%></td>
                <td><%= pedido.getDescripcion()%></td>
                <td style="text-align: right"><%= String.valueOf(pedido.getPrecio())%></td>
                <td style="text-align: right"><%= String.valueOf(pedido.getUnidades())%></td>
                <td style="text-align: right"><%= String.valueOf(pedido.getImporte())%></td>
            </tr>
            <%
                    total = total + pedido.getImporte();
                    i++;
                }
            %>
        </table>
        <hr>
        <h2 style="text-align: right">Total Factura (I.V.A. inc.): <%= String.valueOf(total)%></h2>
        <hr>
    </body>
</html>
