<%-- 
    Document   : buscarClienteExtractos
    Created on : 05-feb-2019, 9:00:25
    Author     : Alex
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="objetos.Cliente"%>
<%@page import="gestorBD.GestorBD"%>
<%--
La nueva página que se cargue, podrá tener dos aspectos (HTML dinámico), dependiendo
de la existencia o inexistencia del código. Si el cliente no existe se mostrará que no existe el cliente.

Desde esta ventana, ejecutando los enlaces insertados en la misma, podemos introducir un
nuevo cliente, volviendo a la ventana de buscar Cliente, o regresar a la página principal.

Si el cliente introducido existe, se mostrará los datos del cliente y la busqueda de extractos entre
dos fechas.Se pueden seleccionar las fechas entre las que el cliente solicita el
extracto de pedidos, introducir un nuevo cliente, volviendo a la pagina de buscar Cliente, o
regresar a la página principal.

En esta ventana, se incluyen listas combinadas para que el usuario seleccione las fechas
entre las que desea el extracto de pedidos. Como vemos en esta ventana, las listas están
inicializadas con unos determinados valores, que serán los siguientes:

- Las listas Año se inicializarán con el año actual y los cinco años anteriores.
- Las listas Mes se inicializarán con los valores de 1 a 12.
- Las listas Día se inicializarán con los valores de 1 a 31.

Las listas Día se tendrán que actualizar cada vez que seleccionemos un nuevo mes o un
nuevo año, dependiendo del mes (meses de 28, 29, 30 ó 31 días) y del año (años
bisiestos) seleccionados.

En este caso voy a utilizar calendarios de HTML5.

Cuando el usuario pulse el botón Aceptar se cargará una nueva ventana en el navegador.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body onload="document.formPedidos.txtArticulo.focus()">
        <h1>Gestión de extractos</h1>
        <%
            String codigo = request.getParameter("txtCodigo");
            HttpSession sesion = request.getSession();
            GestorBD bd = (GestorBD) sesion.getAttribute("conexion");
            bd.conectarDB();
            Cliente cliente = bd.consultarCliente(codigo);
            sesion.setAttribute("cliente", cliente);
            if (cliente != null) {
        %>
        <!-- Muestro los datos del cliente -->
        <h2>Datos del cliente</h2>
        <table>
            <tr>
                <td><b>Código</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;N.I.F.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Nombre</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Apellidos</b></td>                
            </tr>
            <tr>
                <td><%= cliente.getCodigo()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNif()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getNombre()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getApellidos()%></td>
            </tr>
            <tr>
                <td><b>Domicilio</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;C.P.</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Localidad</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;Total</b></td>
            </tr>
            <tr>
                <td><%= cliente.getDomicilio()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getCp()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getLocalidad()%></td>
                <td>&nbsp;&nbsp;&nbsp;<%= cliente.getTotalVentas()%></td>
            </tr>
        </table>
        <br>
        <hr>
        <h2>Fechas del extracto</h2>
        <form action="validarFechasExtractos.jsp" method="post">
            <%
                    Date today = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(today);
                    cal.add(Calendar.YEAR, -5);
                    Date min = cal.getTime();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD");
            %>
            Desde:
            <input type="date" name="calendarioDesde" id="calendarioDesde" autofocus required step="1" min="2015-01-01" max="2020-12-31"/>
            Hasta:
            <input type="date" name="calendarioHasta" id="calendarioHasta" required step="1" min="2015-01-01" max="2020-12-31"/>
            <br><br>
            <button type="submit">Aceptar</button>
        </form>
        <%
        } else {
        %>
        <!-- Muestro que el cliente no existe -->
        <h2>El cliente con código  <%= codigo%> no existe.</h2>
        <%
            }
        %>
        <br>
        <a href="gestionExtractos.jsp">Nuevo cliente</a>
        | 
        <a href="index.jsp">Página principal</a>
    </body>
</html>
