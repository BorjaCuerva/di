package objetos;


/**
 * Clase PedidoInternet. En esta clase crearemos un objeto PedidoInternet con
 * los parametros de la tabla pedidosInternet.
 */
public class PedidoInternet {

    // Parametros
    private String cliente;
    private String articulo;
    private float unidades;
    private String fecha;

    // Constructor defecto
    public PedidoInternet() {
    }

    // Constructor

    public PedidoInternet(String cliente, String articulo, float unidades, String fecha) {
        this.cliente = cliente;
        this.articulo = articulo;
        this.unidades = unidades;
        this.fecha = fecha;
    }


    //Getters and Setters
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public float getUnidades() {
        return unidades;
    }

    public void setUnidades(float unidades) {
        this.unidades = unidades;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


}
