package objetos;

/**
 * Clase Articulo. En esta clase crearemos un objeto Articulo con los parametros
 * de un articulo.
 *
 */
public class Articulo {

    // Parametros
    private String codigoArticulo;
    private String descripcion;
    private float stock;
    private float stockMinimo;
    private float precioCompra;
    private float precioVenta;

    // Constructor
    public Articulo(String codigoArticulo, String descripcion, float stock, float stockMinimo, float precioCompra, float precioVenta) {
        this.codigoArticulo = codigoArticulo;
        this.descripcion = descripcion;
        this.stock = stock;
        this.stockMinimo = stockMinimo;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
    }

    // Getters and Setters
    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }

    public float getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(float stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

}
