package utilidades;

/**
 * Clase Utilidades. En esta clase tenemos los metodos necesarios para la clase
 * grafica.
 */
public class Utilidades {

    /**
     * Metodo calcularLetra. Recibimos como parametro el numero del dni,
     * calculamos el resultado de los numeros para obtener la letra del Dni.
     *
     * @param dni
     * @return
     */
    public static char calcularLetra(int dni) {
        String juegoCaracteres = "TRWAGMYFPDXBNJZSQVHLCKE";
        int modulo = dni % 23;
        char letra = juegoCaracteres.charAt(modulo);
        return letra;
    }

    /**
     * Metodo cerosIzquierda. Recibimos las posiciones que queremos que sea de
     * largo nuestro String y le aniadimos los ceros que faltan al String
     * (texto) que recibimos.
     *
     * @param posiciones
     * @param texto
     * @return
     */
    public static String cerosIzquierda(int posiciones, String texto) {
        while (texto.length() < posiciones) {
            texto = "0" + texto;
        }
        return texto;
    }
}
