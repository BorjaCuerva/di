	
 
<%@page import="objetos.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page import="gestorBD.GestorBD"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    // Creamos un HttpSession con la conexion a la BBDD
    HttpSession httpSession = request.getSession(true);
    // Creamos el GestorBD
    GestorBD bd = new GestorBD();
    // Aniadimos el GestorBD al HttpSesion 
    httpSession.setAttribute("conexion", bd);

    httpSession.setAttribute("pedidos", new ArrayList<Pedido>());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página principal</title>
    </head>
    <body>
        <h1>Proyecto Web con Base de Datos</h1>
        <a href="gestionPedidos.jsp">Gestión de Pedidos</a>
    </body>
</html>
