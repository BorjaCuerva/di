package gestorBD;

import objetos.Cliente;

/**
 * Interface Persistencia. Metodos que debemos implementar en la clase GestorBD.
 */
public interface Persistencia {

    // Conecta la BD.
    void conectarDB();

    // Desconecta la BD.
    void desconectarDB();

    // Alta de cliente introducido.
    void altaCliente(Cliente c);

    // Modifica el cliente introducido.
    void modificarCliente(Cliente c);

    // Baja del cliente introducido.
    void bajaCliente(Cliente c);

    // Consulta si el cliente existe o no, por el codigo introducido.
    Cliente consultarCliente(String codigo);
}
