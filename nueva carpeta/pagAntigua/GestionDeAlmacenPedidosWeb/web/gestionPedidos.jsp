<%-- 
En esta ventana, se puede introducir un código de cliente o regresar a la página principal.

Cuando se cargue esta página, el foco se enviará a la caja de texto del Código de cliente.

El botón Aceptar es un botón submit y el botón Cancelar es un botón reset.

Cuando el usuario pulse el botón Cancelar, el foco se enviará a la caja de texto del Código.

Si el usuario no ha introducido ningún dato y pulsa el botón Aceptar, se visualizará un
mensaje de error y continuaremos en la misma página.

Cuando el usuario haya introducido un código y pulse el botón Aceptar, se cargará una
nueva página en el navegador.

Si el usuario pulsa el enlace Página principal, se cargará la página principal.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <!-- Carga la pagina con el foco en el codigo -->
    <body onload="document.formulario.txtCodigo.focus()">
        <h1>Gestión de pedidos</h1>
        <br>
        <form action="buscarCliente.jsp" name="formulario" method="post" onsubmit="return correctoSubmit()">
            Código de cliente
            <input type="text" name="txtCodigo" pattern="[A-z0-9]{0,6}" maxlength="6" size="6">
            <br><br>
            <input type="submit" name="botonAceptar" value="Aceptar" accesskey="A">
            <input type="reset" name="botonCancelar" value="Cancelar" accesskey="C" onclick="document.formulario.txtCodigo.focus()">
            <br><br>
            <!-- Enlace pagina principal -->
            <a href="index.jsp">Página principal</a>
        </form>
        <script type="text/javascript">
            // Metodo para comprobar que se ha introducido un numero
            function correctoSubmit() {
                with (document.formulario.txtCodigo) {
                    if (document.formulario.txtCodigo.value === "") {
                        alert("No ha introducido ningún código");
                        focus();
                        return false;
                    } else {
                        for (i = 1; value.length < 6; i++) {
                            value = "0" + value;
                        }
                        return true;
                    }
                }
            }
        </script>
    </body>
</html>
